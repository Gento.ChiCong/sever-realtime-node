const mongoose = require('mongoose');
const mongoDBConfigs = require('../configs/configs').mongoDB;
const HOST = mongoDBConfigs.host;
const PORT = mongoDBConfigs.port;
const DB = mongoDBConfigs.database;
const username = mongoDBConfigs.username;
const password = mongoDBConfigs.password;
const connectionString = `mongodb://${username}:${password}@${HOST}:${PORT}/${DB}`;

const opt = {
  useNewUrlParser: true
};

// console.log('MongoDB: ', connectionString);
mongoose.connect(connectionString, opt);

// console.log('readyState: ', mongoose.connection.readyState);
// check status connect: mongoose.connection.readyState
// 0: disconnected
// 1: connected
// 2: connecting
// 3: disconnecting

const db = {};

db.DataRealTime = require('./schemas/data_realtime');
db.Logger = require('./schemas/logger');

module.exports = db;