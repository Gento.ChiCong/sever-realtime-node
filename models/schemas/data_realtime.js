let Promise= require('bluebird');
let mongoose = require('mongoose');
mongoose.Promise = Promise;
let Schema = mongoose.Schema;
const models = 'data_realtime';

const realtimeSchema = new Schema({
  id: {
    type: String,
    required: true
  },
  refPath: {
    type: String,
    required: true
  },
  keyName: {
    type: String,
    required: true
  },
  value: {
    type: String
  },
  nodeType: {
    type: String,
    required: true
  },
  level: {
    type: Number,
    required: true
  },
  listParentRef: {
    type: []
  },
  parentId: {
    type: String
  }
});

module.exports = mongoose.model(models, realtimeSchema);
