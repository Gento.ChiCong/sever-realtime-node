/**
 * Created by lee on 2/11/19.
 */
let Promise= require('bluebird');
let mongoose = require('mongoose');
mongoose.Promise = Promise;
let Schema = mongoose.Schema;
const models = 'logger';

const loggerSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  functionName: {
    type: String
  },
  hint: {
    type: String
  },
  content: {
    type: []
  },
  createAt: {
    type: Date,
    required : true
  }
});

module.exports = mongoose.model(models, loggerSchema);
