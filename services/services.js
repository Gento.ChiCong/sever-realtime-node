const mongodb = require('../models/mongoose.index');
const uuid = require('uuid');
const _ = require('lodash');

module.exports = {
  getAllRoot(){
    return new Promise((resolve, reject) => {
      mongodb.DataRealTime.find(
        { 'parentId': { $exists: false } },
        {
          '_id': 0,
          'id': 1,
          'refPath': 1,
          'keyName': 1,
          'value': 1,
          'nodeType': 1,
          'level': 1,
        }
      ).then(allRoot => {
        const rs = allRoot.map((item) => {
          const i = {};
          i.id = item.id;
          i.refPath = item.refPath;
          i.keyName = item.keyName;
          i.value = item.value;
          i.nodeType = item.nodeType;
          i.level = item.level;
          i.clicked = false;
          i.spaces = [];
          for(let j=0; j<i.level; j++){
            i.spaces.push(1);
          }
          return i;
        });
        resolve(rs);
      }).catch(error => {
        this.writeLog('error', 'getAllRoot', 'H0011', error);
        reject(error);
      });
    });
  },

  getDirectChild(nodeId){
    return new Promise((resolve, reject) => {
      mongodb.DataRealTime.find(
        { parentId: nodeId},
        {
          '_id': 0,
          'id': 1,
          'refPath': 1,
          'keyName': 1,
          'value': 1,
          'nodeType': 1,
          'level': 1,
        }
      ).then(listChild => {
        const rs = listChild.map((item) => {
          const i = {};
          i.id = item.id;
          i.refPath = item.refPath;
          i.keyName = item.keyName;
          i.value = item.value;
          i.nodeType = item.nodeType;
          i.level = item.level;
          i.clicked = false;
          i.spaces = [];
          for(let j=0; j<i.level; j++){
            i.spaces.push(1);
          }
          return i;
        });
        resolve(rs);
      }).catch(error => {
        this.writeLog('error', 'getDirectChild', 'H0012', error);
        reject(error);
      });
    });
  },

  updateNode: function (refPath, stringValue) {
    return new Promise((resolve, reject) => {
      Promise.all([
        mongodb.DataRealTime.findOne({ refPath })
      ]).then(existed => {
        if(existed[0]) {
          // Node existed now can update value
          if (existed[0].nodeType === 'leaf') {
            // Just accept update value for node leaf
            Promise.all([
              mongodb.DataRealTime.updateOne({refPath}, {$set: {value: stringValue}})
            ]).then(updated => {
              resolve({error: false, msg: 'Updated value success!'});
            }).catch(error => {
              this.writeLog('error', 'updateNode', 'Cannot Update Node Leaf', error);
              reject(error);
            });
          } else {
            reject({error: true, msg: 'Only accept update on Leaf Node'});
          }
        }else {
          // Create node leaf and it's parent node
          if(refPath.length > 0 && refPath.substr(0, 1) === '_') {
            const arrayKey = refPath.substr(1).split('_');
            let ref = '';
            // First check which node is exist which is not
            const arrayPromises = [];
            const arrayNodeStatus = [];
            const listParentRef = [];
            _.each(arrayKey, (item) => {
              ref += '_' + item;
              listParentRef.push({ 'ref' : ref });
              const parentList = _.concat([], listParentRef);
              parentList.pop();
              arrayNodeStatus.push({
                refPath: ref,
                status: false,
                keyName: item,
                listParentRef: parentList,
                id: uuid()
              });
              arrayPromises.push(mongodb.DataRealTime.findOne({refPath: ref}));
            });
            Promise.all(arrayPromises).then(results => {
              // Check does any parent is leaf -> wrong ref
              let haveLeafParent = false;
              _.each(results, (item, ind) => {
                if(item){
                  if(item.nodeType === 'leaf') {
                    haveLeafParent = true;
                  }
                  arrayNodeStatus[ind].status = true;
                  arrayNodeStatus[ind].id = item.id;
                }
              });
              if(haveLeafParent === false){
                // No leaf parent now create all parent node
                let insertMany = [];
                _.each(arrayNodeStatus, (item, ind)=>{
                  if(item.status === false) {
                    if (ind != arrayNodeStatus.length - 1) {
                      // Create node parent
                      if(ind === 0){
                        const objectCreate = {
                          id: item.id,
                          refPath: item.refPath,
                          keyName: item.keyName,
                          value: null,
                          nodeType: 'node',
                          level: ind,
                          listParentRef: []
                        };
                        insertMany.push(objectCreate);
                      }else{
                        const objectCreate = {
                          id: item.id,
                          refPath: item.refPath,
                          keyName: item.keyName,
                          value: null,
                          nodeType: 'node',
                          level: ind,
                          listParentRef: item.listParentRef,
                          parentId: arrayNodeStatus[ind-1].id
                        };
                        insertMany.push(objectCreate);
                      }
                    }else{
                      // Create node leaf
                      const objectCreate = {
                        id: item.id,
                        refPath: item.refPath,
                        keyName: item.keyName,
                        value: stringValue,
                        nodeType: 'leaf',
                        level: ind,
                        listParentRef: item.listParentRef,
                        parentId: arrayNodeStatus[ind-1].id
                      };
                      insertMany.push(objectCreate);
                    }
                  }
                });
                Promise.all([
                  mongodb.DataRealTime.insertMany(insertMany)
                ]).then(doneCreate => {
                  resolve({error: false, msg: 'Created new node success.'});
                }).catch(error => {
                  this.writeLog('error', 'updateNode', 'Cannot Create Parent Node', error);
                  reject({error: true, details: error});
                });
              }else {
                reject({error: true, msg: 'Cannot create child of leaf node'});
              }
            }).catch( error => {
              this.writeLog('error', 'updateNode', 'H0001', error);
              reject({error: true, details: error});
            });
          }else {
            reject({error: true, msg: 'Ref not correct format'});
          }
        }
      }).catch(error => {
        this.writeLog('error', 'updateNode', 'H0002', error);
        reject(e);
      });
    });
  },

  checkAndSetupDestination(refPath){
    return new Promise((resolve, reject) => {
      mongodb.DataRealTime.findOne({refPath}).then(existed => {
        console.log('existed: ', existed);
        if (existed) {
          resolve({ existed: true });
        } else {
          this.updateNode(refPath, '').then(data => {
            if (!data.error) {
              resolve({ existed: true });
            }
          }).catch( error => {
            this.writeLog('error', 'checkAndSetupDestination', 'H0003', error);
            reject(e);
          });
        }
      }).catch(error => {
        this.writeLog('error', 'checkAndSetupDestination', 'H0004', error);
        reject(e);
      });
    });
  },

  getSingleNode(nodeId){
    return new Promise((resolve, reject) => {
      mongodb.DataRealTime.findOne({id: nodeId}).then(existed => {
        resolve(existed);
      }).catch(error => {
        this.writeLog('error', 'getSingleNode', 'H0005', error);
        reject(error);
      });
    });
  },

  removeNode(nodeId, phase){
    return new Promise((resolve, reject) => {
      mongodb.DataRealTime.findOne({id: nodeId}).then(existed => {
        console.log('first existed: ', existed);
        if (existed) {
          // First phase remove all children of this node
          if(phase === 'remove-child') {
            if (existed.nodeType === 'node') {
              // List all children
              mongodb.DataRealTime.find({ 'listParentRef.ref': existed.refPath }, { id: 1, _id: 0 }).then(childes => {
                const listIds = _.map(childes, (item) => {
                  return item.id;
                });
                // Add id of current node to remove list
                listIds.push(existed.id);
                // Now remove all node in listIds
                mongodb.DataRealTime.deleteMany({ id:{ $in: listIds } }).then((err, removed) => {
                  if (existed.parentId) {
                    // Recursive to find and remove parent node with no children
                    this.removeNode(existed.parentId, 'remove-parent').then(result => {
                      resolve(result);
                    });
                  }else {
                    // This case will never happen but careful not harm :v !
                    resolve({msg: 'done'});
                  }
                }).catch(error => {
                  this.writeLog('error', 'removeNode', 'H0006', error);
                  reject(error);
                });
              }).catch(error => {
                this.writeLog('error', 'removeNode', 'H0007', error);
                reject(error);
              });
            } else {
              // If current Node is Leaf then remove it
              mongodb.DataRealTime.deleteMany({ id: existed.id }).then((err, removed) => {
                // Recursive to find and remove parent node with no children
                this.removeNode(existed.parentId, 'remove-parent').then(result => {
                  resolve(result);
                });
              }).catch(error => {
                this.writeLog('error', 'removeNode', 'H0008', error);
                reject(error);
              });
            }
          } else {
            // Phase two Recursive to find and remove parent node with no children
            mongodb.DataRealTime.find({ 'parentId': nodeId }, { id: 1 }).then(childes => {
              if (childes.length > 0) {
                // Stop point
                resolve({msg: 'done'});
              }else {
                // Remove current Node and recursive to find and remove parent node with no children
                mongodb.DataRealTime.deleteMany({ id: existed.id }).then((err, removed) => {
                  if(existed.parentId) {
                    this.removeNode(existed.parentId, 'remove-parent').then(result => {
                      // Stop point
                      resolve(result);
                    });
                  }else{
                    // Stop point
                    resolve({msg: 'done'});
                  }
                }).catch(error => {
                  this.writeLog('error', 'removeNode', 'H0007', error);
                  reject(error);
                });
              }
            }).catch(error => {
              this.writeLog('error', 'removeNode', 'H0008', error);
              reject(error);
            });
          }
        } else {
          reject({error: true, msg: 'node not existed'});
        }
      }).catch(error => {
        this.writeLog('error', 'removeNode', 'H0009', error);
        reject(error);
      });
    });
  },

  getRefSnapshot: function (refPath) {
    return new Promise((resolve, reject) => {
      Promise.all([
        mongodb.DataRealTime.findOne({refPath}, {
          listParentRef: 1,
          id: 1,
          refPath: 1,
          keyName: 1,
          value: 1,
          nodeType: 1,
          level:1,
        }),
        mongodb.DataRealTime.find({'listParentRef.ref': refPath}, {
          listParentRef: 1,
          id: 1,
          refPath: 1,
          keyName: 1,
          value: 1,
          nodeType: 1,
          level: 1,
          parentId: 1,
        }).sort({level: 1}),
      ]).then( results => {
        if (results[0]) {
          let array = _.concat(results[0],results[1]);
          const list = JSON.parse(JSON.stringify(array));
          // Convert data to array parent and child
          const rs = this.arrayRelations(list);
          // Continue convert array parent & child to json object
          const json = this.convertArrayToJson(rs);
          // console.log('test: ', JSON.stringify(json));
          resolve(json);
        }else {
          resolve(null);
        }
      }).catch(error => {
        this.writeLog('error', 'getRefSnapshot', 'H0010', error);
        reject(error);
      });
    });
  },

  // Return an array of tree with relationship
  arrayRelations(data) {
    let tree = [], childrenOf = {}, item, id, parentId;
    for (let i = 0, length = data.length; i < length; i++) {
      item = data[i];
      id = item['id'];
      parentId = item['parentId'];
      // Every item may have children
      childrenOf[id] = childrenOf[id] || [];
      // Init its children
      item['children'] = childrenOf[id];
      if (parentId) {
        // Init its parent's children object
        childrenOf[parentId] = childrenOf[parentId] || [];
        // Push it into its parent's children object
        childrenOf[parentId].push(item);
      } else {
        tree.push(item);
      }
    }
    return tree;
  },

  // Convert array with tree relations to json object
  convertArrayToJson(array){
    const returnObject = {};
    _.each(array, item => {
      if(item.children.length > 0){
        returnObject[item.keyName] = this.convertArrayToJson(item.children);
      }else {
        returnObject[item.keyName] = item.value;
      }
    });
    return returnObject;
  },

  // Write log for debugging
  writeLog(type, functionName, hint, error){
    const content = [];
    content.push(error);
    mongodb.Logger.create({ type, functionName, hint, content, createAt: new Date()}).then( crt => {
      console.log(crt);
    }).catch( error => {
      console.log(error);
    });
  }
};