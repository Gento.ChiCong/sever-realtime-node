const _ = require('lodash');
const services = require('./services');
/** RabbitMQ Init Connect **/
const amqp = require('amqplib/callback_api');
const CLOUDAMQP_URL = 'amqp://adminrabbit:p121rabbit@127.0.0.1:5672';
var amqpConn = null;
// var channel = null;

module.exports = {
  start() {
    amqp.connect(CLOUDAMQP_URL, function(err, conn) {
      amqpConn = conn;
      // conn.createChannel((err, ch) => {
      //   channel = ch;
        // let q = 'test_exchange';
        // ch.assertQueue(q, { durable: false });
        // const exchanges = 'logs';
        // ch.assertExchange(exchanges, 'fanout', {durable: false});

        // let counter = 0;
        // setInterval(() => {
        //   counter += 1;
        //   let msg = 'Message ' + counter + 'th';
        //   // ch.sendToQueue(q, Buffer.from(msg));
        //   console.log('send to Queue ' + counter);
        //   ch.publish(exchanges, '', new Buffer(msg));
        // }, 20000);
      // });
    });
  },

  publishAllParent(refPath){
    if(refPath.length > 0 && refPath.substr(0, 1) === '_') {
      const arrayKey = refPath.substr(1).split('_');
      // console.log('arrayKey: ', arrayKey);
      let exchange= '';
      _.each(arrayKey, (item) => {
        exchange += '_' + item;
        const ref = exchange;
        amqpConn.createChannel((err, channel) => {
          channel.assertExchange(ref, 'fanout', { durable: false });
          services.getRefSnapshot(ref).then((snapshot) => {
            const st = JSON.stringify(snapshot);
            channel.publish(ref, '', new Buffer(st));
          });
        });
      });
    }
  },

  assertAllParent(refPath){
    return new Promise( (resolve, reject) => {
      if(refPath.length > 0 && refPath.substr(0, 1) === '_') {
        const arrayKey = refPath.substr(1).split('_');
        let exchange= '';
        amqpConn.createChannel((err, channel) => {
          _.each(arrayKey, (item) => {
            exchange += '_' + item;
            const ref = exchange;
            channel.assertExchange(ref, 'fanout', { durable: false });
          });
          resolve(true);
        });
      }else {
        reject({msg: 'ref incorrect'});
      }
    });
  }
};