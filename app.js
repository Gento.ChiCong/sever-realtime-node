const port = 9696;
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const router = express.Router();

const services = require('./services/services');
const rabbitmq = require('./services/rabbitmq');

function defaultContentTypeMiddleware (req, res, next) {
    next();
}

const corsWhiteList = [
  'file://',
  'http://localhost',
  'http://35.189.40.243',
  'https://35.189.40.243',
  'https://pos.butleric.com',
  'http://pos.butleric.com',
  'https://frontpos.butleric.com',
  'http://frontpos.butleric.com',
  'https://gentochicong.github.io',
  'http://gentochicong.github.io',
];
const corsOpts = {
  origin: (origin, next) => {
    if (origin && corsWhiteList.indexOf(origin) === -1) {
      next(new Error('Not allowed by CORS.'));
    }
    next(null, true);
  },
  credentials: true,
};

app.use(cors(corsOpts));

app.use(defaultContentTypeMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('port', port);

rabbitmq.start();

/** ----------------------- **/

router.post('/get-ref-snapshot',
  function(req, res) {
    const { refPath } = req.body;
    services.getRefSnapshot(refPath).then( data => {
      res.json({error: false, data});
    }).catch( e => {
      res.json({error: true, details: e});
    });

  }
);

// Because socket at client will been drop if client subscribe a not existed exchange
// So we must check if not existed then create it with null value
router.post('/check-and-setup-destination',
  function(req, res) {
    const { refPath } = req.body;
    services.checkAndSetupDestination(refPath).then( existed => {
      rabbitmq.assertAllParent(refPath).then(done => {
        res.json({error: false, data: existed});
      }).catch(e => {
        console.log('e1: ', e);
        res.json({error: true, details: e});
      });
    }).catch( e => {
      console.log('e2: ', e);
      res.json({error: true, details: e});
    });
  }
);

// This apis will create or update all node and leaf of one refPath
router.post('/set-node',
  function(req, res) {
    const { refPath, stringValue } = req.body;
    services.updateNode(refPath, stringValue).then( data => {
      if (!data.error) {
        rabbitmq.publishAllParent(refPath);
      }
      res.json({error: false, data});
    }).catch( e => {
      console.log('e: ', e);
      res.json({error: true, details: e});
    });
  }
);

// This apis will create or update all node and leaf of one refPath
router.post('/remove-node',
  function(req, res) {
    const { nodeId } = req.body;
    services.getSingleNode(nodeId).then(node => {
      console.log('Target node removed: ', node);
      if(node) {
        services.removeNode(nodeId, 'remove-child').then( data => {
          if (!data.error) {
            rabbitmq.publishAllParent(node.refPath);
          }
          res.json({error: false, data});
        }).catch( e => {
          res.json({error: true, details: e});
        });
      }else {
        res.json({error: true, msg: 'No node exists'});
      }
    }).catch( e => {
      res.json({error: true, details: e});
    });
  }
);

/** ----------------------- **/

router.get('/get-child/:nodeId',
  function(req, res) {
    const { nodeId } = req.params;
    services.getDirectChild(nodeId).then( data => {
      res.json({error: false, data});
    }).catch( e => {
      res.json({error: true, details: e});
    });

  }
);

router.get('/get-all-root',
  function(req, res) {
    services.getAllRoot().then( data => {
      res.json({error: false, data});
    }).catch( e => {
      res.json({error: true, details: e});
    });

  }
);

app.use('/apis', router);

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

